CLI

BOA PRÁTICA DA VIDA:
nomes de pasta e arquivos:
  - minusculas (ver exceções de nomes de classe)
  - sem caracteres especiais
  - sem espaços
  - se precisar, hífen

. - pasta local
.. - pasta anterior
.nome - arquivos iniciados com ponto são ocultos por definição
&& - executa comandos em sequência
& executa comandos em paralelo
-r - recursive
-f - forçar
man <comando> - "manual" do comando 

cd .. / cd ~ / cd - / cd .

ls - lista
ls -l - lista em lista
ls -lha - a: arquivos ocultos / h: hyperlinks

mv - mover 
  mv origem/arquivo.xis destino/
    move o arquivo com o mesmo nome para a outra pasta
  mv origem/arquivo.xis destino/arquivo.ipsilon
    move o arquivo e renomeia
  mv origem/pasta destino/

cp - copiar
  idem mv, só muda:
    cp -r - copiar pastas com conteúdo dentro


rm - deletar
  rm -r (apaga pasta e o que tem dentro)
  rm -rf (força a remoção de tudo)

touch - cria arquivo ou pasta

cat - ler/concatenar arquivos
  1 arquivo: lista o conteúdo
  n arquivos: lista o conteúdo de todos, concatenados
    exemplo: cat client/js/app/models/Negociacao.js  client/index.html > oi.txt
    concatena o conteúdos dos 2 arquivos em um oi.txt
less - leitura de arquivo
  less oi.txt
  pesquisa por termos com /termo
  funciona com regex

vim
  i - editar
  esc - sair dos modos
  /<termo><enter> - pesquisar
  G - vai pra última linha
  gg - vai pra primeira linha
  dd - deletar linha
  d<numero><enter> - deleta número de linhas
  <numero><enter> - pula para linha
  :q - sair (! no final para forçar)
  :wq - salvar e sair (! no final para forçar)
  :w - salvar (! no final para forçar)